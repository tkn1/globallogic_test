This is a test for GlobalLogic. Source code is located in *.h and *.c files.
Tests are located in test/test.cpp.
# Build
```
mkdir build
make conf
make build
```

# Test
```
make test
```
or for verbous output
```
make vtest
```

# Format with ClangFormat
```
make format
```

# Tasks

Please write code for algorithms that act as described below. Please write the code so that it compiles on the following website - https://repl.it/languages/c.
When writing the implementation please keep maintainability and ease to change in mind. The task conditions might change in the future as well. Please have in mind MISRA, code style, naming. Write the code as good as possible.


## Task 1


A random ASCII character is received every 100ms. We need to find a sequence of "GLROX" characters. There can be other characters in between! Once found- call a Found function and wait for the sequence again, if not found then wait until this sequence is found. If a new character does not come in 2 seconds call Timeout function and wait for the whole "GLROX" again even if previously received part of the sequence. The Process function which is your entry point is called every 100ms.



Expected results:

Input | Output
--- | --- |
GabfdbLbsfbdRbasgOX |TRUE
AA4hfghdAAGLROX	|TRUE
GLRObas832hXbasb	|TRUE
GLROobiapjso83basb	|FALSE (no X)
glroxbpoaijspoiasjdb	|FALSE (lower case)
GLpboiasjdbpoijOX	|FALSE (no R)


## Task 2
The code should take integers from defined range e.g. 0-100 and process them as follows.


- If a number is divisible by 3, print “Global”
- If a number is divisible by 5, print “Logic”
- If a number is divisible by 3 AND by 5, print “GlobalLogic” in a single line
- Each printout should be done on a separate line.


Expected results:  


Input	|Output
---|---
0	|0
1	|1
2	|2
3	|Global
4	|4
5	|Logic
6	|Global
7	|7
8	|8
9	|Global
10	|Logic
11	|11
12	|Global
13	|13
14	|14
15	|GlobalLogic
...	|...
100	|Logic

