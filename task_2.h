#ifndef TASK_2
#define TASK_2

#include <stdint.h>
#include <stdio.h>

const char *get_output(uint8_t number);
void print(uint8_t number);

#endif