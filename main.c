#include "task_1.h"
#include "task_2.h"
#include <stdio.h>
#include <unistd.h>

/**
 * @brief Main tests are located in test/test.cpp. This file is only to test https://replit.com/ compatibility.
 * 
 */

uint8_t is_found = 0;
uint8_t is_timeout = 0;

void found_callback(void *data)
{
  (void)(data);
  is_found = 1;
}

void timeout_callback(void *data)
{
  (void)(data);
  is_timeout = 1;
}

void task_1_test()
{
  is_found = 0;
  is_timeout = 0;
  register_found_cb(found_callback, NULL);
  register_timeout_cb(timeout_callback, NULL);
  const char *sequence[] = {"GabfdbLbsfbdRbasgOX", "AA4hfghdAAGLROX", "GLRObas832hXbasb", "GLROobiapjso83basb", "glroxbpoaijspoiasjdb", "GLpboiasjdbpoijOX"};
  uint8_t results[] = {1, 1, 1, 0, 0, 0};

  for (uint32_t sequence_index = 0; sequence_index < sizeof(sequence) / sizeof(char *); sequence_index++)
  {
    for (uint32_t i = 0; i < strlen(sequence[sequence_index]); i++)
    {
      process(sequence[sequence_index][i]);
      usleep(100 * 1000);
    }
    if (is_found != results[sequence_index])
    {
      printf("error!!!\n");
    }
    else
    {
      printf("true!!!\n");
    }
    reset();
    is_found = 0;
    is_timeout = 0;
  }
}

void task_2_test()
{
  for (uint8_t i = 0; i <= 100; i++)
  {
    print(i);
  }
}

int main(void)
{
  task_1_test();
  task_2_test();
  return 0;
}