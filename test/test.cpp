#include <gtest/gtest.h>
#include <iostream>
#include <unistd.h>
extern "C"
{
#include "../task_1.h"
#include "../task_2.h"
}

bool is_found = false;
bool is_timeout = false;

void found_callback(void *data)
{
  (void)(data);
  // std::cout << "found!!" << std::endl;
  is_found = true;
}

void timeout_callback(void *data)
{
  (void)(data);
  // std::cout << "timeout!!" << std::endl;
  is_timeout = true;
}

TEST(task_1, main)
{
  is_found = false;
  is_timeout = false;
  register_found_cb(found_callback, NULL);
  register_timeout_cb(timeout_callback, NULL);
  const char *sequence[] = {"GabfdbLbsfbdRbasgOX", "AA4hfghdAAGLROX", "GLRObas832hXbasb", "GLROobiapjso83basb", "glroxbpoaijspoiasjdb", "GLpboiasjdbpoijOX"};
  uint8_t results[] = {true, true, true, false, false, false};

  for (uint32_t sequence_index = 0; sequence_index < sizeof(sequence) / sizeof(char *); sequence_index++)
  {
    for (uint32_t i = 0; i < strlen(sequence[sequence_index]); i++)
    {
      process(sequence[sequence_index][i]);
      usleep(100 * 1000);
    }
    ASSERT_EQ(is_found, results[sequence_index]);
    reset();
    is_found = false;
    is_timeout = false;
  }
}

TEST(task_1, timeout)
{
  is_found = false;
  is_timeout = false;
  register_found_cb(found_callback, NULL);
  register_timeout_cb(timeout_callback, NULL);
  const char sequence[] = "GabfdbLbsfbdRbasgOX";
  for (uint32_t i = 0; i < strlen(sequence); i++)
  {
    if (i == 3)
    {
      usleep(2100 * 1000);
    }
    else
    {
      usleep(100 * 1000);
    }
    process(sequence[i]);
    if (i == 3)
    {
      ASSERT_EQ(is_timeout, true);
      is_timeout = false;
    }
    else
    {
      ASSERT_EQ(is_timeout, false);
    }
  }
  reset();
  is_found = false;
  is_timeout = false;
}

TEST(task_2, main)
{
  for (uint8_t i = 0; i <= 100; i++)
  {
    print(i);
  }
}