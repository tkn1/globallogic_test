#ifndef TASK_1
#define TASK_1

#include <stdint.h>

typedef void (*callback_func)(void *data);
struct callback
{
  callback_func cf;
  void *data;
};

void reset();
void process(char sym);
void found();
void timeout();
void register_found_cb(callback_func cb, void *data);
void register_timeout_cb(callback_func cb, void *data);

#endif