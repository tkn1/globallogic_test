#include "task_1.h"
#include <stdio.h>
#include <time.h>

static const char SEQUENCE[] = "GLROX";
static size_t current_index_of_sequence = 0;
static time_t last_sym_timestamp = 0;

static void reset_search();
static char current_char_to_find();

static struct callback found_callback = {NULL, NULL};
static struct callback timeout_callback = {NULL, NULL};

static char current_char_to_find()
{
  return SEQUENCE[current_index_of_sequence];
}

static void reset_search()
{
  current_index_of_sequence = 0;
}

void process(char sym)
{
  time_t current_timestamp = time(NULL);
  if ((current_timestamp - last_sym_timestamp >= 2) && (last_sym_timestamp != 0))
  {
    timeout();
  }
  last_sym_timestamp = current_timestamp;
  if (sym == current_char_to_find())
  {
    current_index_of_sequence++;
  }
  if (current_char_to_find() == '\0')
  {
    found();
  }
}

void found()
{
  if (found_callback.cf != NULL)
  {
    found_callback.cf(found_callback.data);
  }
  reset_search();
}

void timeout()
{
  if (timeout_callback.cf != NULL)
  {
    timeout_callback.cf(timeout_callback.data);
  }
  reset_search();
}

void reset()
{
  reset_search();
}

void register_found_cb(callback_func cb, void *data)
{
  found_callback.cf = cb;
  found_callback.data = data;
}
void register_timeout_cb(callback_func cb, void *data)
{
  timeout_callback.cf = cb;
  timeout_callback.data = data;
}