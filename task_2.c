#include "task_2.h"

const char *get_output(uint8_t number)
{
  const char *output = NULL;
  if ((number % 3 == 0) && (number % 5 == 0))
  {
    output = "GlobalLogic";
  }
  else if (number % 3 == 0)
  {
    output = "Global";
  }
  else if (number % 5 == 0)
  {
    output = "Logic";
  }
  return output;
}

void print(uint8_t number)
{
  const char *output = get_output(number);
  /**
     * @brief 0 is divisible by 3 and 5, so output should be "GlobalLogic" but expected output for input 0 is 0 so there is a special case for that
     * 
     */
  if (number == 0)
  {
    printf("0\n");
    return;
  }
  if (output != NULL)
  {
    printf("%s\n", output);
    return;
  }
  else
  {
    printf("%u\n", number);
    return;
  }
}
